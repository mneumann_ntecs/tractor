# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->

**Features**

- Added `Context<T>#myself_ref` which returns `&Addr<T>`.

**Changes**

- `Context<T>#myself` returns `Addr<T>` instead of `&Addr<T>`.

- Crate `tractor_macros` will treat argument named `ctx` as a `&Context<Self>`.

## [0.0.6](https://gitlab.com/mneumann_ntecs/tractor/-/tree/v0.0.6) - 2021-03-05

**Features**

- Macro `#[actor(derive_hooks)]` will derive default `ActorHooks`.

- Macro `#[async_actor(derive_hooks)]` will derive default `ActorHooksAsync`.

**Changes**

- Split `Actor` into two traits `ActorBehavior` and `ActorHooks`.

- Split `AsyncActor` into two traits `ActorBehaviorAsync` and `ActorHooksAsync`.

- Rename `ActorReceiver` to `Actor`.

- Remove `AsyncActor`.

## [0.0.5](https://gitlab.com/mneumann_ntecs/tractor/-/tree/v0.0.5) - 2021-03-05

**Features**

- Add `MappedChannel` to tranform a `Channel<A>` into a `Channel<B>`.

## [0.0.4](https://gitlab.com/mneumann_ntecs/tractor/-/tree/v0.0.4) - 2021-03-04

**Features**

- Implement `#[async_actor]` macro in `tractor_macros`.

- Added `ActorSystem::run_to_completion` and
  `ActorSystem::run_to_completion_fut`.

- Export common types through `tractor::prelude`.

- Add `AsyncActor` trait.

## [0.0.3](https://gitlab.com/mneumann_ntecs/tractor/-/tree/v0.0.3) - 2021-03-04

**Features**

- Use `tokio` as runtime. In some benchmarks this seems to be twice as fast.

- Use `flume` for channels. Channels in `flume` provide a `len` property. The
  `len` property can be used by the message sender to protect an actor from
  being overloaded.

## [0.0.2](https://gitlab.com/mneumann_ntecs/tractor/-/tree/v0.0.2) - 2021-01-30

**Features**

- Implement `#[actor]` proc macro that generates lots of boiler-plate code.

## [0.0.1](https://gitlab.com/mneumann_ntecs/tractor/-/tree/v0.0.1) - 2021-01-30

**Features**

- Initial realease.

