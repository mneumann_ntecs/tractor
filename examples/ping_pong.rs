use tractor::prelude::*;

const MAX_NUM_PINGS: usize = 10;

/// Ping actor
pub struct Ping {
    pong: Addr<Pong>,
    count: usize,
}

#[actor(derive_hooks)]
impl Ping {
    fn start(&mut self, ctx: &Context<Self>) {
        self.count = 0;
        self.pong.ping(ctx.myself());
    }

    fn pong(&mut self, ctx: &Context<Self>) {
        println!("pong {}", self.count);
        if self.count < MAX_NUM_PINGS {
            self.count += 1;
            self.pong.ping(ctx.myself());
        }
    }
}

/// Pong actor
pub struct Pong;

#[actor(derive_hooks)]
impl Pong {
    fn ping(&mut self, sender: Addr<Ping>) {
        println!("ping");
        sender.pong();
    }
}

fn actor_main() {
    let pong = Pong.start();
    let ping = Ping { pong, count: 0 }.start();
    ping.start();
}

fn main() {
    ActorSystem::run_to_completion(actor_main);
}
