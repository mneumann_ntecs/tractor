use tractor::prelude::*;

enum MyActorMsg {
    Add(usize, usize),
    AddedResult(usize),
}

enum AdderMsg {
    Add {
        a: usize,
        b: usize,
        respond: Chan<MyActorMsg>,
    },
}

struct MyActor {
    adder: Chan<AdderMsg>,
}

impl Actor for MyActor {
    type Msg = MyActorMsg;
}

impl ActorHooks for MyActor {}

impl ActorBehavior for MyActor {
    fn handle(&mut self, msg: MyActorMsg, ctx: &Context<Self>) {
        match msg {
            MyActorMsg::Add(a, b) => {
                self.adder.send(AdderMsg::Add {
                    a,
                    b,
                    respond: ctx.myself_ref().chan(),
                });
            }

            MyActorMsg::AddedResult(result) => {
                println!("Result: {}", result);
            }
        }
    }
}

struct Adder;

impl Actor for Adder {
    type Msg = AdderMsg;
}

impl ActorBehavior for Adder {
    fn handle(&mut self, msg: AdderMsg, _: &Context<Self>) {
        match msg {
            AdderMsg::Add { a, b, respond } => {
                respond.send(MyActorMsg::AddedResult(a + b));
            }
        }
    }
}

impl ActorHooks for Adder {}

fn run() {
    let adder = Adder.start().chan();
    let myactor = MyActor { adder }.start();

    myactor.send(MyActorMsg::Add(1, 2));
    myactor.send(MyActorMsg::Add(1, 5));
}

fn main() {
    ActorSystem::run_to_completion(run);
}
