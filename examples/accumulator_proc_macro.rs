use std::time::Duration;
use tokio::{task, time::sleep};
use tractor::prelude::*;

pub struct Accumulator {
    sum: usize,
}

#[actor(derive_hooks)]
impl Accumulator {
    fn add(&mut self, a: usize) {
        self.sum += a;
    }

    fn sub(&mut self, a: usize) {
        self.sum -= a;
    }
}

trait AccumulatorRefExt: Address<Accumulator> {
    fn overloaded(&self) -> bool {
        self.len() > 1000
    }
}

impl AccumulatorRefExt for Addr<Accumulator> {}

async fn run() {
    let accumulator = Accumulator { sum: 0 }.start();

    let feeder = task::spawn(async move {
        for _i in 0..1_000_000_usize {
            while accumulator.overloaded() {
                sleep(Duration::from_millis(1)).await;
                // println!("Overload");
            }
            accumulator.add(1);
        }
    });

    feeder.await.unwrap();
}

fn main() {
    ActorSystem::run_to_completion_fut(run());
}
