#[cfg(test)]
use tractor::prelude::*;
use tractor_macros::actor;

#[test]
fn actor_attribute_keeps_impl_as_is() {
    pub struct Adder {
        sum: usize,
    }

    #[actor]
    impl Adder {
        fn inc(&mut self) {
            self.sum += 1;
        }

        fn add(&mut self, num: usize) {
            self.sum += num;
        }

        fn sub(&mut self, num: usize) {
            self.sum -= num;
        }

        fn add2(&mut self, a: usize, b: usize) {
            self.sum += a + b;
        }
    }
    impl ActorHooks for Adder {}

    let mut adder = Adder { sum: 123 };
    adder.inc();
}

#[test]
fn actor_attribute_should_create_message_type() {
    pub struct Adder {
        sum: usize,
    }

    #[actor]
    impl Adder {
        fn inc(&mut self) {
            self.sum += 1;
        }

        fn add(&mut self, num: usize) {
            self.sum += num;
        }

        fn sub(&mut self, num: usize) {
            self.sum -= num;
        }

        fn add2(&mut self, a: usize, b: usize) {
            self.sum += a + b;
        }
    }
    impl ActorHooks for Adder {}

    // The `#[actor]` attribute creates the following code:
    //
    // ```rust
    // enum AdderMsg {
    //   Inc { }
    //   Add { num: usize }
    //   Sub { num: usize }
    //   Add2 { a: usize, b: usize }
    // }
    // ```
    //

    let _msg = AdderMsg::Inc {};
    let _msg = AdderMsg::Add { num: 123 };
    let _msg = AdderMsg::Sub { num: 123 };
    let _msg = AdderMsg::Add2 { a: 123, b: 344 };
}

#[tokio::test]
async fn actor_attribute_implements_actor_trait() {
    pub struct Adder {
        sum: usize,
    }

    #[actor]
    impl Adder {
        fn add(&mut self, num: usize) {
            self.sum += num;
        }

        fn sub(&mut self, num: usize) {
            self.sum -= num;
        }
    }
    impl ActorHooks for Adder {}

    // This will create the following code:
    //
    // ```rust
    // enum AdderMsg {
    //   Add { num: usize }
    //   Sub { num: usize }
    // }
    //
    // impl Actor for Adder {
    //   type Msg = AdderMsg;
    //
    //   fn handle(&mut self, msg: AdderMsg, ctx: &Context<Self>) {
    //     match msg {
    //       AdderMsg::Add {num: m_num} => self.add(m_num),
    //       AdderMsg::Sub {num: m_num} => self.sub(m_num),
    //     }
    //   }
    // }
    // ```

    {
        let _addr = Adder { sum: 123 }.start();
    }
    ActorSystem::wait_for_completion().await;
}

#[tokio::test]
async fn actor_attribute_implements_actor_hooks_when_derive_hooks_is_specified() {
    pub struct Adder {
        sum: usize,
    }

    #[actor(derive_hooks)]
    impl Adder {
        fn add(&mut self, num: usize) {
            self.sum += num;
        }

        fn sub(&mut self, num: usize) {
            self.sum -= num;
        }
    }

    // This will create the following code:
    //
    // ```rust
    // enum AdderMsg {
    //   Add { num: usize }
    //   Sub { num: usize }
    // }
    //
    // impl Actor for Adder {
    //   type Msg = AdderMsg;
    //
    //   fn handle(&mut self, msg: AdderMsg, ctx: &Context<Self>) {
    //     match msg {
    //       AdderMsg::Add {num: m_num} => self.add(m_num),
    //       AdderMsg::Sub {num: m_num} => self.sub(m_num),
    //     }
    //   }
    // }
    // ```

    {
        let _addr = Adder { sum: 123 }.start();
    }
    ActorSystem::wait_for_completion().await;
}

#[tokio::test]
async fn actor_attribute_implements_actor_ref_trait() {
    pub struct Adder {
        sum: usize,
    }

    #[actor]
    impl Adder {
        fn add(&mut self, num: usize) {
            self.sum += num;
        }

        fn sub(&mut self, num: usize) {
            self.sum -= num;
        }
    }
    impl ActorHooks for Adder {}

    // This will create the following code:
    //
    // ```rust
    // enum AdderMsg {
    //   Add { num: usize }
    //   Sub { num: usize }
    // }
    //
    // impl Actor for Adder {
    //   type Msg = AdderMsg;
    //
    //   fn handle(&mut self, msg: AdderMsg, ctx: &Context<Self>) {
    //     match msg {
    //       AdderMsg::Add {num: m_num} => self.add(m_num),
    //       AdderMsg::Sub {num: m_num} => self.sub(m_num),
    //     }
    //   }
    // }
    //
    // pub trait AdderRef : tractor::Channel<AdderMsg> {
    //    fn add(&self, num : usize) {
    //      self.send(AdderMsg::Add { num });
    //    }
    //    fn sub(&self, num : usize) {
    //      self.send(AdderMsg::Sub { num });
    //    }
    // }
    // ```

    {
        let adder: Addr<Adder> = Adder { sum: 123 }.start();
        adder.add(1);
        adder.sub(2);
    }

    ActorSystem::wait_for_completion().await;
}

#[tokio::test]
async fn method_definition_supports_context_attribute() {
    pub struct Adder {
        sum: usize,
    }

    #[actor]
    impl Adder {
        fn add(&mut self, num: usize, ctx: &Context<Self>) {
            let _myself = ctx.myself();
            self.sum += num;
        }
        fn sub(&mut self, num: usize) {
            self.sum -= num;
        }
    }
    impl ActorHooks for Adder {}

    // This will create the following code:
    //
    // ```rust
    // enum AdderMsg {
    //   Add { num: usize }
    //   Sub { num: usize }
    // }
    //
    // impl Actor for Adder {
    //   type Msg = AdderMsg;
    //
    //   fn handle(&mut self, msg: AdderMsg, ctx: &Context<Self>) {
    //     match msg {
    //       AdderMsg::Add {num: m_num} => self.add(m_num, ctx),
    //       AdderMsg::Sub {num: m_num} => self.sub(m_num),
    //     }
    //   }
    // }
    //
    // pub trait AdderRef : tractor::Channel<AdderMsg> {
    //    fn add(&self, num : usize) {
    //      self.send(AdderMsg::Add { num });
    //    }
    //    fn sub(&self, num : usize) {
    //      self.send(AdderMsg::Sub { num });
    //    }
    // }
    // ```

    {
        let adder: Addr<Adder> = Adder { sum: 123 }.start();
        adder.add(1);
        adder.sub(2);
    }

    ActorSystem::wait_for_completion().await;
}
