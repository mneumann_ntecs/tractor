use crate::{Actor, Mailbox};
use core::future::Future;
use std::sync::atomic::{AtomicBool, AtomicIsize, Ordering};
use std::time::Duration;
use tokio::{task, time::sleep};

static ACTIVE_ACTORS_COUNT: AtomicIsize = AtomicIsize::new(0);
static REFERENCED_ACTORS_COUNT: AtomicIsize = AtomicIsize::new(0);
static ACTOR_SYSTEM_IS_TERMINATING: AtomicBool = AtomicBool::new(false);

pub struct ActorSystem;

impl ActorSystem {
    pub fn is_terminating() -> bool {
        ACTOR_SYSTEM_IS_TERMINATING.load(Ordering::SeqCst)
    }

    pub fn terminate() {
        ACTOR_SYSTEM_IS_TERMINATING.store(true, Ordering::SeqCst);
    }

    pub(crate) fn spawn_actor<T, F>(future: F)
    where
        T: Actor,
        F: Future<Output = (T, Mailbox<T::Msg>)> + Send + 'static,
    {
        let _ = ACTIVE_ACTORS_COUNT.fetch_add(1, Ordering::SeqCst);
        let _ = REFERENCED_ACTORS_COUNT.fetch_add(1, Ordering::SeqCst);

        let _actor_task = task::spawn(async move {
            let (_actor, mailbox) = future.await;
            let _ = ACTIVE_ACTORS_COUNT.fetch_sub(1, Ordering::SeqCst);

            let () = terminate_actor::<T>(&mailbox, Duration::from_millis(1000)).await;

            // XXX: do something with mailbox!

            REFERENCED_ACTORS_COUNT.fetch_sub(1, Ordering::SeqCst);
        });
    }

    #[tokio::main]
    pub async fn run_to_completion_fut(start_actors: impl std::future::Future<Output = ()>) {
        let () = start_actors.await;
        Self::wait_for_completion().await;
    }

    #[tokio::main]
    pub async fn run_to_completion(start_actors: impl FnOnce()) {
        let () = start_actors();
        Self::wait_for_completion().await;
    }

    pub async fn wait_for_completion() {
        loop {
            let refcnt = REFERENCED_ACTORS_COUNT.load(Ordering::SeqCst);
            if refcnt == 0 {
                break;
            }
            println!("# Refereced actors: {}", refcnt);
            sleep(Duration::from_millis(1000)).await;
        }
    }
}

/// Poll until either
///
/// * no more references to the actor's mailbox exist, OR
///
/// * no more actors are running.
///
async fn terminate_actor<T: Actor>(mailbox: &Mailbox<T::Msg>, poll_interval: Duration) {
    if mailbox.consumed {
        return;
    }

    while ACTIVE_ACTORS_COUNT.load(Ordering::SeqCst) > 0 {
        sleep(poll_interval).await;
    }
}
